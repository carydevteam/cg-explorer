# CG Explorer

A modern web application built with Angular and Spring Boot, visualizing CG campers' check-in statistics

## Getting Started

| | Command | Action |
| --- | --- | --- |
| 1 | `git clone https:<username>.bitbucket.org/carydevteam/cg-explorer.git` | Clone repository to local machine, creating cg-explorer folder |


### Prerequisites

What things you need to install the software and how to install them

| Library | Version | Check Version Command | Additional Commands |
| --- | --- | --- | --- |
| [Node.js](https://nodejs.org/en/download/) | `8.9.1` | `node --version` | |
| NPM | `5.5.1` | `npm --version` | `npm install -g npm@5.5.1` |
| [Maven](https://maven.apache.org/download.cgi) | `3.5.2` | `mvn --version` | |
| [Java](http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html) | `9.0.1` | `java -version` | |


### Installing


#### Front-end 

| | Command | Action |
| --- | --- | --- |
| 1 | `cd cg-explorer` | Move into `cg-explorer` directory |
| 2 | `cd client; npm install` | Move into `client` directory, install all front-end dependencies |

#### Back-end

| | Command | Action |
| --- | --- | --- |
| 1 | `cd cg-explorer` | Move into `cg-explorer` directory |
| 2 | `cd src; mvn install` | Move into `src` directory, install all back-end dependencies |

## Running the tests

#### Front-end

| | Command | Action |
| --- | --- | --- |
| 1 | `cd cg-explorer/client` | Move into `client` directory |
| 2 | `npm test` | Run front-end tests |

#### Back-end

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
